import unittest

# import the function(s) you are testing
from template_module import actual_function

# create a class for the test that implements unittest abstract class
class TestFunction(unittest.TestCase):
    def test_function(self):
        # data to use as a test case
        inputs = {}
        # Expected result
        truth = Null
        self.assertAlmostEqual(actual_function(inputs, ''), truth)

    def test_values(self):
        # make sure value errors are raised when necessary
        bad_input = 'junk'      # known garbage data
        self.assertRaises(ValueError, actual_function, bad_input)
# end class

"""
code to run the unit test
    > python -m unittest test_function
"""