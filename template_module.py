"""
Eric Peters
November 2020
python 3.8.2

This script acts as a template for a CLI program that takes inputs in the form of a 
JSON file, performs calculations, and outputs the results to a file

Usage (from a terminal):
$ python [script_name].py f_in f_out

    f_in: path to JSON file containing inputs
    f_out: path to save results JSON file

Where f_in, f_out have the following structures
    f_in:
        {
            var1:value,
            var2:value,
            ...
        }
    f_out: 
        {
            metadata{
                script_name: "",
                run_date: [date and time the script was run in YYYY-MM-DD HH:mm:ss Timezone format]
            },
            input{
                //copy of input variable(S)
            },
            output{
                //result(s)
            }
        }

"""
import logging, argparse, json, platform
from datetime import datetime as dt
from pathlib import Path

def main():
    # parse command-line arguments
    parser = argparse.ArgumentParser(description="Template CLI Program")
    parser.add_argument('f_in', type=str,
                        help='Path to JSON file containing inputs')
    parser.add_argument('f_out', type=str,
                        help='Path to desired output file')
    command_args = parser.parse_args()
    # for debugging
    # TODO replace with logging module
    print(command_args)

    # TODO check if supplied strings are valid paths
    # f_in
    f_in = command_args.f_in
    # f_out
    f_out = command_args.f_out

    # read JSON data from f_in
    with open(f_in) as inputfile:
        inputs = json.load(inputfile)
    # file closed

    # create dict of metadata
    # create timestamp for output file metadata
    script = Path(__file__).name
    timestamp = dt.now().strftime("%Y-%b-%d %H:%M:%S")
    version = platform.python_version()
    metadata = dict(script_name=script,
                    run_time=timestamp,
                    python=version)

    # pass input data and output file name to function
    actual_function(metadata, inputs, f_out)
#end main

def actual_function(metadata, inputs, f_out):
    # do stuff
    print("Supplied inputs:")
    print(json.dumps(inputs,sort_keys=True, indent=4))

    # fill this with whatever values this script is supposed to calculate 
    outputs = dict()

    # write everything to output file
    with open(f_out,'w', encoding="utf-8") as out:
        json.dump({"metadata":metadata,"inputs":inputs, "outputs":outputs},
                out, sort_keys=False, indent=4)
    # file closed
# end actual_function

if __name__ == "__main__":
    main()
# end