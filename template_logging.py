"""
template_logging.py

Template file containing code snippets for implementing Python logging module in a function

Logging levels and associated integers:
    NOTSET - 0
    DEBUG - 10
    INFO - 20
    WARNING - 30
    ERROR - 40
    CRITICAL - 50
"""

""" in header"""
import logging

# create and configure logger using basicConfig method
LOG_FORMAT = '%(levelname)s %(asctime)s - %(message)s'
logfile = r'path\to\logfile.txt' #TODO implement Path module
logging.basicConfig(filename=logfile,
                    level=logging.DEBUG,
                    format = LOG_FORMAT)
logger = logging.getLogger()


"""Inside function """ 
logger.debug('Debug message')
logger.info('Info message')
logger.warning('Warning message')
logger.error('Error message!')
logger.critical('Critical message!!')