import logging

class TemplateClass:
    # define variables common to all instances of class
    # var1
    # var2
    def __init__(self):
        # define variables specific to each instance
        # self.var3 = ...
        pass
    # end __init__

    def main():
        """ Description of main program function. """
        print("Hello world")
    # end main()

    if __name__ == "__main__":
        main()
    # end if
# end class template